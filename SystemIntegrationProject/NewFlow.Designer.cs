﻿namespace SystemIntegrationProject
{
    partial class NewFlow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxInputs = new System.Windows.Forms.ComboBox();
            this.comboBoxOutputs = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelInputExample = new System.Windows.Forms.Label();
            this.labelOptional = new System.Windows.Forms.Label();
            this.textBoxValueSplitter = new System.Windows.Forms.TextBox();
            this.textBoxObjectSplitter = new System.Windows.Forms.TextBox();
            this.labelValueSplitter = new System.Windows.Forms.Label();
            this.labelObjectSplitter = new System.Windows.Forms.Label();
            this.labelDomainSplitters = new System.Windows.Forms.Label();
            this.labelTopic = new System.Windows.Forms.Label();
            this.comboBoxTableRequestType = new System.Windows.Forms.ComboBox();
            this.labelTableRequestType = new System.Windows.Forms.Label();
            this.comboBoxTableType = new System.Windows.Forms.ComboBox();
            this.labelTableType = new System.Windows.Forms.Label();
            this.buttonSelectInputFilePath = new System.Windows.Forms.Button();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.labelInput = new System.Windows.Forms.Label();
            this.richTextBoxRestInputExample = new System.Windows.Forms.RichTextBox();
            this.textBoxKeys = new System.Windows.Forms.TextBox();
            this.textBoxTopic = new System.Windows.Forms.TextBox();
            this.labelKeys = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxRestOutputType = new System.Windows.Forms.ComboBox();
            this.labelRestOutputType = new System.Windows.Forms.Label();
            this.labelOutputHTML = new System.Windows.Forms.Label();
            this.textBoxOutputFileName = new System.Windows.Forms.TextBox();
            this.labelOutputFileName = new System.Windows.Forms.Label();
            this.buttonSelectOutputFilePath = new System.Windows.Forms.Button();
            this.textBoxOutputFilePath = new System.Windows.Forms.TextBox();
            this.labelOutput = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.buttonExtract = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input";
            // 
            // comboBoxInputs
            // 
            this.comboBoxInputs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInputs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxInputs.FormattingEnabled = true;
            this.comboBoxInputs.Location = new System.Drawing.Point(136, 22);
            this.comboBoxInputs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxInputs.Name = "comboBoxInputs";
            this.comboBoxInputs.Size = new System.Drawing.Size(299, 33);
            this.comboBoxInputs.TabIndex = 1;
            this.comboBoxInputs.SelectedIndexChanged += new System.EventHandler(this.comboBoxInputs_SelectedIndexChanged);
            // 
            // comboBoxOutputs
            // 
            this.comboBoxOutputs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOutputs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOutputs.FormattingEnabled = true;
            this.comboBoxOutputs.Location = new System.Drawing.Point(913, 22);
            this.comboBoxOutputs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxOutputs.Name = "comboBoxOutputs";
            this.comboBoxOutputs.Size = new System.Drawing.Size(299, 33);
            this.comboBoxOutputs.TabIndex = 3;
            this.comboBoxOutputs.SelectedIndexChanged += new System.EventHandler(this.comboBoxOutputs_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(789, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "Output";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelInputExample);
            this.groupBox1.Controls.Add(this.labelOptional);
            this.groupBox1.Controls.Add(this.textBoxValueSplitter);
            this.groupBox1.Controls.Add(this.textBoxObjectSplitter);
            this.groupBox1.Controls.Add(this.labelValueSplitter);
            this.groupBox1.Controls.Add(this.labelObjectSplitter);
            this.groupBox1.Controls.Add(this.labelDomainSplitters);
            this.groupBox1.Controls.Add(this.labelTopic);
            this.groupBox1.Controls.Add(this.comboBoxTableRequestType);
            this.groupBox1.Controls.Add(this.labelTableRequestType);
            this.groupBox1.Controls.Add(this.comboBoxTableType);
            this.groupBox1.Controls.Add(this.labelTableType);
            this.groupBox1.Controls.Add(this.buttonSelectInputFilePath);
            this.groupBox1.Controls.Add(this.textBoxInput);
            this.groupBox1.Controls.Add(this.labelInput);
            this.groupBox1.Controls.Add(this.richTextBoxRestInputExample);
            this.groupBox1.Controls.Add(this.textBoxKeys);
            this.groupBox1.Controls.Add(this.textBoxTopic);
            this.groupBox1.Controls.Add(this.labelKeys);
            this.groupBox1.Location = new System.Drawing.Point(17, 90);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(565, 466);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // labelInputExample
            // 
            this.labelInputExample.AutoSize = true;
            this.labelInputExample.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInputExample.Location = new System.Drawing.Point(17, 89);
            this.labelInputExample.Name = "labelInputExample";
            this.labelInputExample.Size = new System.Drawing.Size(152, 25);
            this.labelInputExample.TabIndex = 16;
            this.labelInputExample.Text = "Output Example";
            this.labelInputExample.Visible = false;
            // 
            // labelOptional
            // 
            this.labelOptional.AutoSize = true;
            this.labelOptional.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelOptional.Location = new System.Drawing.Point(225, 235);
            this.labelOptional.Name = "labelOptional";
            this.labelOptional.Size = new System.Drawing.Size(79, 20);
            this.labelOptional.TabIndex = 24;
            this.labelOptional.Text = "(optional)";
            this.labelOptional.Visible = false;
            // 
            // textBoxValueSplitter
            // 
            this.textBoxValueSplitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxValueSplitter.Location = new System.Drawing.Point(164, 265);
            this.textBoxValueSplitter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxValueSplitter.Name = "textBoxValueSplitter";
            this.textBoxValueSplitter.Size = new System.Drawing.Size(47, 30);
            this.textBoxValueSplitter.TabIndex = 23;
            // 
            // textBoxObjectSplitter
            // 
            this.textBoxObjectSplitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxObjectSplitter.Location = new System.Drawing.Point(164, 228);
            this.textBoxObjectSplitter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxObjectSplitter.Name = "textBoxObjectSplitter";
            this.textBoxObjectSplitter.Size = new System.Drawing.Size(47, 30);
            this.textBoxObjectSplitter.TabIndex = 22;
            // 
            // labelValueSplitter
            // 
            this.labelValueSplitter.AutoSize = true;
            this.labelValueSplitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValueSplitter.Location = new System.Drawing.Point(13, 268);
            this.labelValueSplitter.Name = "labelValueSplitter";
            this.labelValueSplitter.Size = new System.Drawing.Size(128, 25);
            this.labelValueSplitter.TabIndex = 21;
            this.labelValueSplitter.Text = "Value Splitter";
            this.labelValueSplitter.Visible = false;
            // 
            // labelObjectSplitter
            // 
            this.labelObjectSplitter.AutoSize = true;
            this.labelObjectSplitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelObjectSplitter.Location = new System.Drawing.Point(13, 231);
            this.labelObjectSplitter.Name = "labelObjectSplitter";
            this.labelObjectSplitter.Size = new System.Drawing.Size(134, 25);
            this.labelObjectSplitter.TabIndex = 20;
            this.labelObjectSplitter.Text = "Object Splitter";
            this.labelObjectSplitter.Visible = false;
            // 
            // labelDomainSplitters
            // 
            this.labelDomainSplitters.AutoSize = true;
            this.labelDomainSplitters.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.labelDomainSplitters.Location = new System.Drawing.Point(17, 197);
            this.labelDomainSplitters.Name = "labelDomainSplitters";
            this.labelDomainSplitters.Size = new System.Drawing.Size(213, 31);
            this.labelDomainSplitters.TabIndex = 19;
            this.labelDomainSplitters.Text = "Domain Splitters";
            this.labelDomainSplitters.Visible = false;
            // 
            // labelTopic
            // 
            this.labelTopic.AutoSize = true;
            this.labelTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopic.Location = new System.Drawing.Point(13, 111);
            this.labelTopic.Name = "labelTopic";
            this.labelTopic.Size = new System.Drawing.Size(61, 25);
            this.labelTopic.TabIndex = 15;
            this.labelTopic.Text = "Topic";
            this.labelTopic.Visible = false;
            // 
            // comboBoxTableRequestType
            // 
            this.comboBoxTableRequestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTableRequestType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTableRequestType.FormattingEnabled = true;
            this.comboBoxTableRequestType.Location = new System.Drawing.Point(241, 169);
            this.comboBoxTableRequestType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxTableRequestType.Name = "comboBoxTableRequestType";
            this.comboBoxTableRequestType.Size = new System.Drawing.Size(176, 33);
            this.comboBoxTableRequestType.TabIndex = 14;
            this.comboBoxTableRequestType.Visible = false;
            // 
            // labelTableRequestType
            // 
            this.labelTableRequestType.AutoSize = true;
            this.labelTableRequestType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTableRequestType.Location = new System.Drawing.Point(16, 172);
            this.labelTableRequestType.Name = "labelTableRequestType";
            this.labelTableRequestType.Size = new System.Drawing.Size(195, 25);
            this.labelTableRequestType.TabIndex = 13;
            this.labelTableRequestType.Text = "One Request each ...";
            this.labelTableRequestType.Visible = false;
            // 
            // comboBoxTableType
            // 
            this.comboBoxTableType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTableType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTableType.FormattingEnabled = true;
            this.comboBoxTableType.Location = new System.Drawing.Point(241, 129);
            this.comboBoxTableType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxTableType.Name = "comboBoxTableType";
            this.comboBoxTableType.Size = new System.Drawing.Size(176, 33);
            this.comboBoxTableType.TabIndex = 12;
            this.comboBoxTableType.Visible = false;
            // 
            // labelTableType
            // 
            this.labelTableType.AutoSize = true;
            this.labelTableType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTableType.Location = new System.Drawing.Point(17, 139);
            this.labelTableType.Name = "labelTableType";
            this.labelTableType.Size = new System.Drawing.Size(173, 25);
            this.labelTableType.TabIndex = 11;
            this.labelTableType.Text = "Table Head is in ...";
            this.labelTableType.Visible = false;
            // 
            // buttonSelectInputFilePath
            // 
            this.buttonSelectInputFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectInputFilePath.Location = new System.Drawing.Point(473, 30);
            this.buttonSelectInputFilePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelectInputFilePath.Name = "buttonSelectInputFilePath";
            this.buttonSelectInputFilePath.Size = new System.Drawing.Size(69, 32);
            this.buttonSelectInputFilePath.TabIndex = 9;
            this.buttonSelectInputFilePath.Text = "...";
            this.buttonSelectInputFilePath.UseVisualStyleBackColor = true;
            this.buttonSelectInputFilePath.Click += new System.EventHandler(this.buttonSelectInputFilePath_Click);
            // 
            // textBoxInput
            // 
            this.textBoxInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInput.Location = new System.Drawing.Point(119, 30);
            this.textBoxInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.Size = new System.Drawing.Size(348, 30);
            this.textBoxInput.TabIndex = 10;
            this.textBoxInput.TextChanged += new System.EventHandler(this.textBoxInput_TextChanged);
            this.textBoxInput.Leave += new System.EventHandler(this.textBoxInput_Leave);
            // 
            // labelInput
            // 
            this.labelInput.AutoSize = true;
            this.labelInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInput.Location = new System.Drawing.Point(17, 32);
            this.labelInput.Name = "labelInput";
            this.labelInput.Size = new System.Drawing.Size(43, 25);
            this.labelInput.TabIndex = 9;
            this.labelInput.Text = "File";
            // 
            // richTextBoxRestInputExample
            // 
            this.richTextBoxRestInputExample.Location = new System.Drawing.Point(21, 129);
            this.richTextBoxRestInputExample.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBoxRestInputExample.Name = "richTextBoxRestInputExample";
            this.richTextBoxRestInputExample.Size = new System.Drawing.Size(520, 317);
            this.richTextBoxRestInputExample.TabIndex = 15;
            this.richTextBoxRestInputExample.Text = "";
            this.richTextBoxRestInputExample.Visible = false;
            // 
            // textBoxKeys
            // 
            this.textBoxKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKeys.Location = new System.Drawing.Point(89, 154);
            this.textBoxKeys.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxKeys.Name = "textBoxKeys";
            this.textBoxKeys.Size = new System.Drawing.Size(319, 30);
            this.textBoxKeys.TabIndex = 18;
            this.textBoxKeys.Text = "Enter keys splitted by ( ; )";
            // 
            // textBoxTopic
            // 
            this.textBoxTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTopic.Location = new System.Drawing.Point(89, 105);
            this.textBoxTopic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxTopic.Name = "textBoxTopic";
            this.textBoxTopic.Size = new System.Drawing.Size(319, 30);
            this.textBoxTopic.TabIndex = 16;
            // 
            // labelKeys
            // 
            this.labelKeys.AutoSize = true;
            this.labelKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKeys.Location = new System.Drawing.Point(13, 158);
            this.labelKeys.Name = "labelKeys";
            this.labelKeys.Size = new System.Drawing.Size(57, 25);
            this.labelKeys.TabIndex = 17;
            this.labelKeys.Text = "Keys";
            this.labelKeys.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxRestOutputType);
            this.groupBox2.Controls.Add(this.labelRestOutputType);
            this.groupBox2.Controls.Add(this.labelOutputHTML);
            this.groupBox2.Controls.Add(this.textBoxOutputFileName);
            this.groupBox2.Controls.Add(this.labelOutputFileName);
            this.groupBox2.Controls.Add(this.buttonSelectOutputFilePath);
            this.groupBox2.Controls.Add(this.textBoxOutputFilePath);
            this.groupBox2.Controls.Add(this.labelOutput);
            this.groupBox2.Location = new System.Drawing.Point(644, 90);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(568, 466);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // comboBoxRestOutputType
            // 
            this.comboBoxRestOutputType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRestOutputType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxRestOutputType.FormattingEnabled = true;
            this.comboBoxRestOutputType.Location = new System.Drawing.Point(150, 89);
            this.comboBoxRestOutputType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxRestOutputType.Name = "comboBoxRestOutputType";
            this.comboBoxRestOutputType.Size = new System.Drawing.Size(299, 33);
            this.comboBoxRestOutputType.TabIndex = 11;
            this.comboBoxRestOutputType.Visible = false;
            // 
            // labelRestOutputType
            // 
            this.labelRestOutputType.AutoSize = true;
            this.labelRestOutputType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRestOutputType.Location = new System.Drawing.Point(19, 94);
            this.labelRestOutputType.Name = "labelRestOutputType";
            this.labelRestOutputType.Size = new System.Drawing.Size(121, 25);
            this.labelRestOutputType.TabIndex = 19;
            this.labelRestOutputType.Text = "Outupt Type";
            this.labelRestOutputType.Visible = false;
            // 
            // labelOutputHTML
            // 
            this.labelOutputHTML.AutoSize = true;
            this.labelOutputHTML.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOutputHTML.Location = new System.Drawing.Point(355, 91);
            this.labelOutputHTML.Name = "labelOutputHTML";
            this.labelOutputHTML.Size = new System.Drawing.Size(53, 25);
            this.labelOutputHTML.TabIndex = 18;
            this.labelOutputHTML.Text = ".html";
            // 
            // textBoxOutputFileName
            // 
            this.textBoxOutputFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOutputFileName.Location = new System.Drawing.Point(125, 89);
            this.textBoxOutputFileName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxOutputFileName.Name = "textBoxOutputFileName";
            this.textBoxOutputFileName.Size = new System.Drawing.Size(223, 30);
            this.textBoxOutputFileName.TabIndex = 17;
            // 
            // labelOutputFileName
            // 
            this.labelOutputFileName.AutoSize = true;
            this.labelOutputFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOutputFileName.Location = new System.Drawing.Point(19, 91);
            this.labelOutputFileName.Name = "labelOutputFileName";
            this.labelOutputFileName.Size = new System.Drawing.Size(100, 25);
            this.labelOutputFileName.TabIndex = 16;
            this.labelOutputFileName.Text = "File Name";
            // 
            // buttonSelectOutputFilePath
            // 
            this.buttonSelectOutputFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectOutputFilePath.Location = new System.Drawing.Point(473, 30);
            this.buttonSelectOutputFilePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelectOutputFilePath.Name = "buttonSelectOutputFilePath";
            this.buttonSelectOutputFilePath.Size = new System.Drawing.Size(69, 32);
            this.buttonSelectOutputFilePath.TabIndex = 15;
            this.buttonSelectOutputFilePath.Text = "...";
            this.buttonSelectOutputFilePath.UseVisualStyleBackColor = true;
            this.buttonSelectOutputFilePath.Click += new System.EventHandler(this.buttonSelectOutputFilePath_Click);
            // 
            // textBoxOutputFilePath
            // 
            this.textBoxOutputFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOutputFilePath.Location = new System.Drawing.Point(125, 30);
            this.textBoxOutputFilePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxOutputFilePath.Name = "textBoxOutputFilePath";
            this.textBoxOutputFilePath.Size = new System.Drawing.Size(343, 30);
            this.textBoxOutputFilePath.TabIndex = 15;
            // 
            // labelOutput
            // 
            this.labelOutput.AutoSize = true;
            this.labelOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOutput.Location = new System.Drawing.Point(19, 30);
            this.labelOutput.Name = "labelOutput";
            this.labelOutput.Size = new System.Drawing.Size(88, 25);
            this.labelOutput.TabIndex = 15;
            this.labelOutput.Text = "File Path";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(17, 614);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(164, 46);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(17, 562);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(551, 46);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "Add Flow";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // buttonExtract
            // 
            this.buttonExtract.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExtract.Location = new System.Drawing.Point(187, 614);
            this.buttonExtract.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonExtract.Name = "buttonExtract";
            this.buttonExtract.Size = new System.Drawing.Size(381, 46);
            this.buttonExtract.TabIndex = 10;
            this.buttonExtract.Text = "Extract";
            this.buttonExtract.UseVisualStyleBackColor = true;
            this.buttonExtract.Click += new System.EventHandler(this.buttonExtract_Click);
            // 
            // NewFlow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1251, 694);
            this.Controls.Add(this.buttonExtract);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboBoxOutputs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxInputs);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "NewFlow";
            this.Text = "New Flow";
            this.Load += new System.EventHandler(this.NewFlow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxInputs;
        private System.Windows.Forms.ComboBox comboBoxOutputs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.Label labelInput;
        private System.Windows.Forms.Button buttonSelectInputFilePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox textBoxOutputFilePath;
        private System.Windows.Forms.Label labelOutput;
        private System.Windows.Forms.TextBox textBoxOutputFileName;
        private System.Windows.Forms.Label labelOutputFileName;
        private System.Windows.Forms.Button buttonSelectOutputFilePath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button buttonExtract;
        private System.Windows.Forms.Label labelOutputHTML;
        private System.Windows.Forms.ComboBox comboBoxTableType;
        private System.Windows.Forms.Label labelTableType;
        private System.Windows.Forms.ComboBox comboBoxTableRequestType;
        private System.Windows.Forms.Label labelTableRequestType;
        private System.Windows.Forms.RichTextBox richTextBoxRestInputExample;
        private System.Windows.Forms.Label labelInputExample;
        private System.Windows.Forms.Label labelTopic;
        private System.Windows.Forms.TextBox textBoxKeys;
        private System.Windows.Forms.Label labelKeys;
        private System.Windows.Forms.TextBox textBoxTopic;
        private System.Windows.Forms.Label labelOptional;
        private System.Windows.Forms.TextBox textBoxValueSplitter;
        private System.Windows.Forms.TextBox textBoxObjectSplitter;
        private System.Windows.Forms.Label labelValueSplitter;
        private System.Windows.Forms.Label labelObjectSplitter;
        private System.Windows.Forms.Label labelDomainSplitters;
        private System.Windows.Forms.Label labelRestOutputType;
        private System.Windows.Forms.ComboBox comboBoxRestOutputType;
    }
}