﻿/**
 * Data Sources Converter
 * Integração de Sistemas
 * Membros - Alexandre Santos, André Silva, César Subtil, Miguel Santos
 * 
 * Table Request Type Enum -> Enumeration with all Table Request Types (Make a Request for a TABLE vs for each LINE of the table)
 **/
namespace SystemIntegrationProject
{
    public enum TableRequestType
    {
        TABLE,
        LINE
    }
}