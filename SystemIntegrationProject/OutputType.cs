﻿/**
 * Data Sources Converter
 * Integração de Sistemas
 * Membros - Alexandre Santos, André Silva, César Subtil, Miguel Santos
 * 
 * Output Type Enum -> Enumeration with all Output Types supported
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemIntegrationProject
{
    public enum OutputType
    {
        HTML,
        RESTAPI
    }
}
