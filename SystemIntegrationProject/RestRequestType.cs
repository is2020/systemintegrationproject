﻿/**
 * Data Sources Converter
 * Integração de Sistemas
 * Membros - Alexandre Santos, André Silva, César Subtil, Miguel Santos
 * 
 * Rest Request Type Enum -> Enumeration with all Rest Request Types (Make a Request for ALL objects or One Per Object)
 **/
namespace SystemIntegrationProject
{
    public enum RestRequestType
    {
        ALL,
        ONE_PER_OBJECT
    }
}