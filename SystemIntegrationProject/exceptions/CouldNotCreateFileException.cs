﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemIntegrationProject.exceptions
{
    class CouldNotCreateFileException : Exception
    {
        public CouldNotCreateFileException(string message) : base(message) { }
    }
}
