﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemIntegrationProject.exceptions
{
    class CouldNotConnectToRemoteException : Exception
    {
        public CouldNotConnectToRemoteException(string message) : base(message) { }
    }
}
