﻿using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Microsoft.VisualBasic;
using SystemIntegrationProject.exceptions;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SystemIntegrationProject
{
    public partial class NewFlow : Form
    {
        public Flow flow;
        

        public NewFlow()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void NewFlow_Load(object sender, EventArgs e)
        {
            #region POPULATE COMBOBOXES
            comboBoxOutputs.DataSource = Enum.GetValues(typeof(OutputType));
            comboBoxInputs.DataSource = Enum.GetValues(typeof(InputType));
            comboBoxTableType.DataSource = Enum.GetValues(typeof(TableType));
            comboBoxTableRequestType.DataSource = Enum.GetValues(typeof(TableRequestType));
            comboBoxRestOutputType.DataSource = Enum.GetValues(typeof(RestRequestType));
            #endregion


            // When the New Flow form loads
            // If the "flow" variable is already filled
            // Then we are in EDIT MODE
            if (flow != null)
            {
                #region MODIFY UI IF IS IN EDIT MODE
                comboBoxInputs.SelectedItem = flow.InputType;
                comboBoxOutputs.SelectedItem = flow.OutputType;
                if (flow.InputType == InputType.RESTAPI)
                {
                    textBoxInput.Text = flow.URL;
                    if (flow.OutputType == OutputType.RESTAPI)
                    {
                        if (flow.RestRequestType == RestRequestType.ALL)
                        {
                            comboBoxRestOutputType.SelectedItem = RestRequestType.ALL;
                        }
                        else
                        {
                            comboBoxRestOutputType.SelectedItem = RestRequestType.ONE_PER_OBJECT;
                        }
                        
                    }
                }
                else
                {
                    textBoxInput.Text = flow.InputFilePath;
                }
                if (flow.OutputType == OutputType.RESTAPI)
                {
                    textBoxOutputFilePath.Text = flow.OutputURL;
                }
                else
                {
                    textBoxOutputFilePath.Text = flow.OutputFilePath;
                }
                if (flow.TableType == TableType.COLUMN)
                {
                    comboBoxTableType.SelectedItem = TableType.COLUMN;
                }
                else
                {
                    comboBoxTableType.SelectedItem = TableType.LINE;
                }
                if (flow.TableRequestType == TableRequestType.LINE)
                {
                    comboBoxTableRequestType.SelectedItem = TableRequestType.LINE;
                }
                else
                {
                    comboBoxTableRequestType.SelectedItem = TableRequestType.TABLE;
                }
                if(flow.InputType == InputType.BROKER)
                {
                    textBoxInput.Text = flow.Domain;
                    textBoxTopic.Text = flow.Topic;
                    string strTemp = "";
                    foreach(string key in flow.Keys)
                    {
                        strTemp += (key + ";");
                    }
                    textBoxKeys.Text = strTemp;
                    if (flow.ObjectSplitter.ToString() != "")
                    {
                        textBoxObjectSplitter.Text = flow.ObjectSplitter.ToString();
                    }
                    textBoxValueSplitter.Text = flow.ValueSplitter.ToString();
                    
                }

                textBoxOutputFileName.Text = flow.OutputFileName;
                buttonSave.Text = "Save Flow";
                #endregion
            }

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBoxInputs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxInputs.SelectedIndex == 2)
            {
                #region MODIFY UI -> RESTFUL API SELECTED
                labelInput.Text = "URL";
                buttonSelectInputFilePath.Visible = false;
                textBoxInput.Size = new Size(320,30);
                if ((OutputType)comboBoxOutputs.SelectedItem == OutputType.RESTAPI)
                {
                    comboBoxRestOutputType.Visible = true;
                    labelRestOutputType.Visible = true;
                }
                #endregion
            }
            else
            {
                #region MODIFY UI -> NOT RESTFUL API SELECTED
                textBoxInput.Size = new Size(260, 30);
                labelInput.Text = "File";
                buttonSelectInputFilePath.Visible = true;
                comboBoxRestOutputType.Visible = false;
                labelRestOutputType.Visible = false;
                labelInputExample.Visible = false;
                richTextBoxRestInputExample.Visible = false;
                #endregion
            }
            if ((InputType)comboBoxInputs.SelectedItem == InputType.EXCEL)
            {
                #region MODIFY UI -> EXCEL SELECTED
                labelTableType.Visible = true;
                comboBoxTableType.Visible = true;

                if ((OutputType)comboBoxOutputs.SelectedItem == OutputType.RESTAPI)
                {
                    #region MODIFY UI -> EXCEL TO RESTAPI SELECTED
                    labelTableRequestType.Visible = true;
                    comboBoxTableRequestType.Visible = true;
                    #endregion
                }

                #endregion
            }
            else
            {
                #region MODIFY UI -> EXCEL NOT SELECTED
                labelTableType.Visible = false;
                comboBoxTableType.Visible = false;
                labelTableRequestType.Visible = false;
                comboBoxTableRequestType.Visible = false;
                #endregion
            }
            if ((InputType)comboBoxInputs.SelectedItem == InputType.BROKER)
            {
                #region MODIFY UI -> BROKER SELECTED
                labelInput.Text = "Domain";
                buttonSelectInputFilePath.Visible = false;
                textBoxInput.Size = new Size(320, 30);
                labelTopic.Visible = true;
                textBoxTopic.Visible = true;
                labelKeys.Visible = true;
                textBoxKeys.Visible = true;
                labelDomainSplitters.Visible = true;
                labelObjectSplitter.Visible = true;
                textBoxObjectSplitter.Visible = true;
                labelValueSplitter.Visible = true;
                textBoxValueSplitter.Visible = true;
                labelOptional.Visible = true;
                #endregion
            }
            else
            {
                #region MODIFY UI -> BROKER NOT SELECTED
                labelTopic.Visible = false;
                textBoxTopic.Visible = false;
                labelKeys.Visible = false;
                textBoxKeys.Visible = false;
                labelDomainSplitters.Visible = false;
                labelObjectSplitter.Visible = false;
                textBoxObjectSplitter.Visible = false;
                labelValueSplitter.Visible = false;
                textBoxValueSplitter.Visible = false;
                labelOptional.Visible = false;
                #endregion
            }
        }
        private void buttonSelectInputFilePath_Click(object sender, EventArgs e)
        {
            #region INPUT FILE PATH
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxInput.Text = openFileDialog.FileName;
            }
            #endregion
        }

        private void buttonSelectOutputFilePath_Click(object sender, EventArgs e)
        {
            #region OUTPUT FILE PATH
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxOutputFilePath.Text = folderBrowserDialog1.SelectedPath;
            }
            #endregion
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!verifyForm())
            {
                return;
            }
            InitializeFlow();

            #region TERMINATE FORM
            this.DialogResult = DialogResult.OK;
            this.Close();
            #endregion
        }

        public static bool WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            bool success = false;
            #region WRITE TO XML
            TextWriter writer = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                writer = new StreamWriter(filePath, append);
                serializer.Serialize(writer, objectToWrite);
                success = true;
                return success;
            }
            catch(Exception e)
            {
                MessageBox.Show($"Error extracting your flow!\n{e.Message}");
                return success;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
            #endregion
        }

        private void buttonExtract_Click(object sender, EventArgs e)
        {
            if (!verifyForm())
            {
                return;
            }
            InitializeFlow();

            #region CREATE SAVE FILE
            string filename = $"../../../Flows/{this.flow.Name}.xml";
            if(WriteToXmlFile<Flow>(filename, flow, false))
            {
                MessageBox.Show($"Extracted Flow {this.flow.Name} with Success to Flows/ in the project directory!");
            }            
            #endregion
        }


        private void InitializeFlow()
        {
            #region FLOW NAME DIALOG

            // Check if this.flow is already initialized
            // If it is... Then the flow already has a name
            // It it isn't, we ask for the flow name

            string flowName;
            if (this.flow != null)
            {
                flowName = this.flow.Name;
            }
            else
            {
                flowName = Interaction.InputBox("Please, specify a name for your flow!", "Flow Name", "Flow 1");
            }
            #endregion

            #region GET URL OR FILE INPUT

            // Check wether the Input Type is a REST API.
            // If it is... Then the textBoxInput is an URL
            // Else it is a FILE PATH...

            string filepath = "";
            string url = "";
            string domain = "";
            string topic = "";
            string[] keys = { };
            char objectSplitter = '\0';
            char valueSplitter = '\0';
            switch ((InputType)comboBoxInputs.SelectedItem)
            {
                case InputType.RESTAPI:
                    url = textBoxInput.Text;
                    break;
                case InputType.EXCEL:
                    filepath = textBoxInput.Text;
                    break;
                case InputType.BROKER:
                    domain = textBoxInput.Text;
                    topic = textBoxTopic.Text;
                    string[] values = textBoxKeys.Text.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    keys = values;
                    if (textBoxObjectSplitter.Text.Length != 0) {
                        objectSplitter = textBoxObjectSplitter.Text[0];
                    }
                    valueSplitter=textBoxValueSplitter.Text[0];
                    break;
            }
            #endregion

            #region GET URL OR FILE OUTPUT

            // Check wether the Output Type is a REST API.
            // If it is... Then the textBoxOutput is an URL
            // Else it is a FILE PATH...

            string filepathOutput = "";
            string urlOutput = "";
            if ((OutputType)comboBoxOutputs.SelectedItem == OutputType.RESTAPI)
            {
                urlOutput = textBoxOutputFilePath.Text;
            }
            else
            {
                filepathOutput = textBoxOutputFilePath.Text;
            }
            #endregion

            #region INITIALIZE CURRENT FLOW
            Flow flow = new Flow
            {
                Name = flowName,
                InputFilePath = filepath,
                InputType = (InputType)comboBoxInputs.SelectedItem,
                OutputType = (OutputType)comboBoxOutputs.SelectedItem,
                TableType = (TableType)comboBoxTableType.SelectedItem,
                TableRequestType = (TableRequestType)comboBoxTableRequestType.SelectedItem,
                RestRequestType = (RestRequestType)comboBoxRestOutputType.SelectedItem,
                OutputFilePath = filepathOutput,
                OutputFileName = textBoxOutputFileName.Text,
                URL = url,
                OutputURL = urlOutput,
                Domain = domain,
                Topic = topic,
                Keys = keys,
                ObjectSplitter = objectSplitter,
                ValueSplitter = valueSplitter
            };

            this.flow = flow;
            #endregion
        }


        private void comboBoxOutputs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((OutputType)comboBoxOutputs.SelectedItem == OutputType.RESTAPI)
            {
                #region MODIFY UI -> RESTFUL API SELECTED
                labelOutput.Text = "URL";
                buttonSelectOutputFilePath.Visible = false;
                textBoxOutputFilePath.Size = new Size(320, 30);
                labelOutputFileName.Visible = false;
                labelOutputHTML.Visible = false;
                textBoxOutputFileName.Visible = false;

                if ((InputType)comboBoxInputs.SelectedItem == InputType.EXCEL)
                {
                    #region MODIFY UI -> EXCEL TO RESTAPI SELECTED
                    labelTableRequestType.Visible = true;
                    comboBoxTableRequestType.Visible = true;
                    #endregion
                }

                if ((InputType)comboBoxInputs.SelectedItem == InputType.RESTAPI)
                {
                    labelRestOutputType.Visible = true;
                    comboBoxRestOutputType.Visible = true;
                }
                else
                {
                    labelRestOutputType.Visible = false;
                    comboBoxRestOutputType.Visible = false;
                }

                #endregion
            }
            else
            {
                #region MODIFY UI -> NOT RESTFUL API SELECTED
                textBoxOutputFilePath.Size = new Size(260, 30);
                labelOutput.Text = "File Path";
                buttonSelectOutputFilePath.Visible = true;
                labelOutputFileName.Visible = true;
                labelOutputHTML.Visible = true;
                textBoxOutputFileName.Visible = true;
                labelTableRequestType.Visible = false;
                comboBoxTableRequestType.Visible = false;
                labelRestOutputType.Visible = false;
                comboBoxRestOutputType.Visible = false;
                #endregion
            }
        }

        private void textBoxInput_TextChanged(object sender, EventArgs e)
        {
            
        }

        private async void textBoxInput_Leave(object sender, EventArgs e)
        {
            if (comboBoxInputs.SelectedIndex == 2 && textBoxInput.Text != "")
            {
                richTextBoxRestInputExample.Visible = true;
                labelInputExample.Visible = true;
                #region URL Definition
                var response = await Task.Run(() =>
                {
                    Uri uri = new Uri(textBoxInput.Text);

                    // Host part
                    string baseUrl = uri.GetLeftPart(UriPartial.Authority);

                    // Other part
                    string paramUrl = uri.AbsolutePath;
                    #endregion

                    #region Setup RestSharp
                    var client = new RestClient(baseUrl);

                    var request = new RestRequest(paramUrl, Method.GET)
                    {
                        OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; }
                    };
                    #endregion

                    var response1 = client.Execute(request);
                    if (response1.StatusCode == 0)
                    {
                        throw new CouldNotConnectToRemoteException($"Could Not Connect to {textBoxInput.Text}");
                    }
                    
                    return response1;
                });
              
                richTextBoxRestInputExample.Text = JToken.Parse(response.Content).ToString(Formatting.Indented);
            }
            else
            {
                richTextBoxRestInputExample.Visible = false;
                labelInputExample.Visible = false;
            }
        }

        private Boolean verifyForm()
        {
            switch (comboBoxInputs.SelectedItem)
            {
                case InputType.BROKER:
                    if (textBoxValueSplitter.Text.Length < 1)
                    {
                        MessageBox.Show("Value splitter is required");
                        return false;
                    }
                    if (textBoxValueSplitter.Text.Length > 1)
                    {
                        MessageBox.Show("Value splitter must be only one character");
                        return false;
                    }
                    if (textBoxInput.Text.Length < 1)
                    {
                        MessageBox.Show("Broker Domain is required");
                        return false;
                    }
                    if (textBoxTopic.Text.Length < 1)
                    {
                        MessageBox.Show("Topics are required");
                        return false;
                    }
                    if (textBoxTopic.Text.Length < 1)
                    {
                        MessageBox.Show("Topics are required");
                        return false;
                    }
                    if (textBoxKeys.Text.Length < 1)
                    {
                        MessageBox.Show("Keys are required");
                        return false;
                    }
                    if (textBoxObjectSplitter.Text.Length > 1)
                    {
                        MessageBox.Show("Object splitter must be only one character");
                        return false;
                    }
                    break;

                case InputType.RESTAPI:
                    if (textBoxInput.Text.Length < 1)
                    {
                        MessageBox.Show("Restful API URL is required");
                        return false;
                    }
                    break;
                case InputType.EXCEL:
                    if (textBoxInput.Text.Length < 1)
                    {
                        MessageBox.Show("File is required");
                        return false;
                    }
                    if (Path.GetExtension(textBoxInput.Text) != ".xls" && Path.GetExtension(textBoxInput.Text) != ".xlsx")
                    {
                        MessageBox.Show("File must be a .xls or .xlsx file type");
                        return false;
                    }
                    break;
            }

            switch (comboBoxOutputs.SelectedItem)
            {
                case OutputType.HTML:
                    if (textBoxOutputFilePath.Text.Length < 1)
                    {
                        MessageBox.Show("File path is required");
                        return false;
                    }
                    if (textBoxOutputFileName.Text.Length < 1)
                    {
                        MessageBox.Show("Filename is required");
                        return false;
                    }
                    break;
                case OutputType.RESTAPI:
                    if (textBoxOutputFilePath.Text.Length < 1)
                    {
                        MessageBox.Show("URL is required");
                        return false;
                    }
                    break;
            }
            
            return true;
        }
    }
}
