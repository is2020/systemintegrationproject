﻿/**
 * Data Sources Converter
 * Integração de Sistemas
 * Membros - Alexandre Santos, André Silva, César Subtil, Miguel Santos
 * 
 * Flow Class -> Stores Information About A Flow
 **/



using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SystemIntegrationProject.exceptions;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Excel = Microsoft.Office.Interop.Excel;

namespace SystemIntegrationProject
{
    public class Flow
    {
        public string Name { get; set; }
        public InputType InputType { get; set; }
        public OutputType OutputType { get; set; }
        public TableType TableType { get; set; }
        public TableRequestType TableRequestType { get; set; }
        public RestRequestType RestRequestType { get; set; }
        public string InputFilePath { get; set; }
        public string URL { get; set; }
        public string OutputFilePath { get; set; }
        public string OutputFileName { get; set; }
        public string OutputURL { get; set; }

        #region Exclusive for Broker
        public string Domain { get; set; }
        public string Topic { get; set; }
        public string[] Keys { get; set; }
        public char ValueSplitter { get; set; }
        public char ObjectSplitter { get; set; }
        public Boolean isRunning { get; set; }

        private MqttClient broker;
        private StreamWriter sw;
        #endregion

        public override string ToString()
        {
            if (Name.Length > 10)
            {
                return $"{Name.Substring(0, 10)}... -     {InputType} \t → \t {OutputType}";
            }
            return $"{Name} -     {InputType} \t → \t {OutputType}";

        }

        public void Run()
        {
            isRunning = false;
            switch (InputType)
            {
                case InputType.EXCEL: 
                    treatExcel();
                    break;
                case InputType.BROKER:
                    RunBroker();
                    break;
                default:
                    TreatRestAPI();
                    break;
            }
        }


        private void TreatRestAPI()
        {

            #region URL Definition
            Uri uri = new Uri(URL);

            // Host part
            string baseUrl = uri.GetLeftPart(UriPartial.Authority);

            // Other part
            string paramUrl = uri.AbsolutePath;
            #endregion

            #region Setup RestSharp
            var client = new RestClient(baseUrl);

            var request = new RestRequest(paramUrl, Method.GET)
            {
                OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; }
            };
            #endregion

            var response = client.Execute(request);
            if (response.StatusCode == 0)
            {
                throw new CouldNotConnectToRemoteException($"Flow {Name} --- Could Not Connect to {URL}");
            }
            var json = new JsonDeserializer().Deserialize<List<Dictionary<string, object>>>(response);

            if (OutputType == OutputType.HTML)
            {
                #region HTML OUTPUT TREATMENT
                var generatedHtml = "";


                #region Treat response
                generatedHtml += "<table style='border: 1px solid black; margin-left: auto; margin-right: auto; border-collapse: collapse;'><tr>";
                foreach (string key in json[0].Keys)
                {
                    generatedHtml += $"<th style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{key}</th>";
                }
                generatedHtml += "</tr>";
                foreach (Dictionary<string, object> item in json)
                {
                    generatedHtml += "<tr>";
                    foreach (KeyValuePair<string, object> item2 in item)
                    {
                        generatedHtml += $"<td style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{item2.Value}</td>";
                    }
                    generatedHtml += "</tr>";
                }
                generatedHtml += "</table>";
                #endregion



                TreatHTML(generatedHtml);

                #endregion
            }
            else if (OutputType == OutputType.RESTAPI)
            {
                #region RESTAPI OUTPUT TREATMENT
                switch (RestRequestType)
                {
                    case RestRequestType.ALL:
                        TreatRestAPIOutput(response.Content);
                        break;
                    case RestRequestType.ONE_PER_OBJECT:
                        foreach (Dictionary<string, object> item in json)
                        {
                            var serializedObject = JsonConvert.SerializeObject(item);
                            TreatRestAPIOutput(serializedObject);
                        }
                        break;
                }
                
                #endregion
            }

        }

        private void TreatHTML(string html)
        {
            string fileToSave = $"{OutputFilePath}\\{OutputFileName}.html";

            #region CREATE AND WRITE TO FILE
            try
            {
                using (StreamWriter sw = File.CreateText(fileToSave))
                {
                    sw.WriteLine(html);
                }
            }
            catch (Exception)
            {
                throw new CouldNotCreateFileException($"Flow {Name} --- Could Not Create HTML File...");
            }
            #endregion
        }

        private void TreatRestAPIOutput(dynamic json)
        {
            #region URL Definition
            Uri uri = new Uri(OutputURL);

            // Host part
            string baseUrl = uri.GetLeftPart(UriPartial.Authority);

            // Other part
            string paramUrl = uri.AbsolutePath;
            #endregion

            #region Setup RestSharp
            var client = new RestClient(baseUrl);

            var request = new RestSharp.RestRequest(uri, Method.POST);

            request.AddJsonBody(json);
            #endregion

            #region RestSharp EXECUTE POST
            IRestResponse response = client.Execute<IRestResponse>(request);
            if (response.StatusCode == 0)
            {
                throw new CouldNotConnectToRemoteException($"Flow {Name} --- Could Not Connect to {OutputURL}");
            }

            // If response != 201 (CREATED)
            // Then the operation was FAILED
            if (response.StatusCode != System.Net.HttpStatusCode.OK && response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new CouldNotCreateResourceException($"Flow {Name} --- Could Not Create Resource");
            }

            #endregion

        }

        private void treatExcel()
        {
            #region OPEN EXCEL FILE

            var excelApplication = new Excel.Application();
            excelApplication.Visible = false;

            var excelWorkbook = excelApplication.Workbooks.Open(InputFilePath);

            #endregion

            try
            {

                if (OutputType == OutputType.HTML)
                {
                    #region HTML OUTPUT TREATMENT
                    var generatedHtml = "";

                    switch (TableType)
                    {
                        case TableType.COLUMN:

                            #region Read Every Worksheet for Column

                            foreach (Excel.Worksheet ws in excelWorkbook.Sheets)
                            {
                                #region Check if valid and get keys

                                int column, tableStartLine = 1;
                                string cell = "";

                                for (column = 1; column < 31; column++)
                                {
                                    for (tableStartLine = 1; tableStartLine < 31; tableStartLine++)
                                    {
                                        cell = ws.Cells[tableStartLine, column].Text;

                                        if (cell != "")
                                        {
                                            break;
                                        }
                                    }
                                    if (cell != "")
                                    {
                                        break;
                                    }
                                }

                                string key = "placeholder";
                                LinkedList<string> keys = new LinkedList<string>();

                                int line = tableStartLine;

                                while (key != "")
                                {

                                    key = ws.Cells[line, column].Text;

                                    keys.AddLast(key);

                                    line++;

                                }

                                keys.RemoveLast();

                                if (keys.Count < 2)
                                {
                                    //INVALID
                                    continue;
                                }

                                column++;

                                LinkedList<string> values = getExcelColumnValues(ws, column, tableStartLine, keys.Count);

                                if (values == null)
                                {
                                    //INVALID
                                    continue;
                                }

                                #endregion

                                #region Format HTML

                                generatedHtml += $"<h1 style='text-align: center'>{ws.Name}</h1> <br><table style='border: 1px solid black; margin-left: auto; margin-right: auto; border-collapse: collapse;'><tr>";

                                for (int i = 0; i < keys.Count; i++)
                                {
                                    generatedHtml += $"<th style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{keys.ElementAt(i)}</th>";
                                }

                                bool first = true;

                                while (true)
                                {
                                    if (!first)
                                    {
                                        values = null;

                                        values = getExcelColumnValues(ws, column, tableStartLine, keys.Count);

                                        if (values == null)
                                        {
                                            break;
                                        }
                                    }

                                    generatedHtml += $"<tr>";

                                    foreach (string value in values)
                                    {
                                        generatedHtml += $"<td style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{value}</td>";
                                    }

                                    generatedHtml += $"</tr>";

                                    if (first)
                                    {
                                        first = false;
                                    }

                                    column++;
                                }

                                generatedHtml += "</table><br><br><br>";

                                #endregion

                            }

                            #endregion

                            break;
                        case TableType.LINE:

                            #region Read Every Worksheet for Line

                            foreach (Excel.Worksheet ws in excelWorkbook.Sheets)
                            {
                                #region Check if valid and get keys

                                int line, tableStartColumn = 1;
                                string cell = "";

                                for (line = 1; line < 31; line++)
                                {
                                    for (tableStartColumn = 1; tableStartColumn < 31; tableStartColumn++)
                                    {
                                        cell = ws.Cells[line, tableStartColumn].Text;

                                        if (cell != "")
                                        {
                                            break;
                                        }
                                    }
                                    if (cell != "")
                                    {
                                        break;
                                    }
                                }

                                string key = "placeholder";
                                LinkedList<string> keys = new LinkedList<string>();

                                //int i = 1;

                                int column = tableStartColumn;

                                while (key != "")
                                {

                                    //key = ws.Cells[1, i].Text;

                                    key = ws.Cells[line, column].Text;

                                    keys.AddLast(key);
                                    //i++;
                                    column++;

                                }

                                keys.RemoveLast();

                                if (keys.Count < 2)
                                {
                                    //INVALID
                                    continue;
                                }

                                line++;

                                LinkedList<string> values = getExcelLineValues(ws, line, tableStartColumn, keys.Count);

                                if (values == null)
                                {
                                    //INVALID
                                    continue;
                                }

                                #endregion

                                #region Format HTML

                                generatedHtml += $"<h1 style='text-align: center'>{ws.Name}</h1> <br><table style='border: 1px solid black; margin-left: auto; margin-right: auto; border-collapse: collapse;'><tr>";

                                for (int i = 0; i < keys.Count; i++)
                                {
                                    generatedHtml += $"<th style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{keys.ElementAt(i)}</th>";
                                }

                                bool first = true;

                                while (true)
                                {
                                    if (!first)
                                    {
                                        values = null;

                                        values = getExcelLineValues(ws, line, tableStartColumn, keys.Count);

                                        if (values == null)
                                        {
                                            break;
                                        }
                                    }

                                    generatedHtml += $"<tr>";

                                    foreach (string value in values)
                                    {
                                        generatedHtml += $"<td style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{value}</td>";
                                    }

                                    generatedHtml += $"</tr>";

                                    if (first)
                                    {
                                        first = false;
                                    }

                                    line++;
                                }

                                generatedHtml += "</table><br><br><br>";

                                #endregion

                            }

                            #endregion

                            break;
                        default:
                            throw new ArgumentException("Invalid Table Type");
                    }

                    TreatHTML(generatedHtml);

                    #endregion

                }
                else if (OutputType == OutputType.RESTAPI)
                {

                    #region JSON TREATMENT

                    switch (TableType)
                    {
                        case TableType.COLUMN:

                            #region Read Every Worksheet for Column

                            foreach (Excel.Worksheet ws in excelWorkbook.Sheets)
                            {
                                #region Check if valid and get keys

                                int column, tableStartLine = 1;
                                string cell = "";

                                for (column = 1; column < 31; column++)
                                {
                                    for (tableStartLine = 1; tableStartLine < 31; tableStartLine++)
                                    {
                                        cell = ws.Cells[tableStartLine, column].Text;

                                        if (cell != "")
                                        {
                                            break;
                                        }
                                    }
                                    if (cell != "")
                                    {
                                        break;
                                    }
                                }

                                string key = "placeholder";
                                LinkedList<string> keys = new LinkedList<string>();

                                int line = tableStartLine;

                                while (key != "")
                                {

                                    key = ws.Cells[line, column].Text;

                                    keys.AddLast(key);

                                    line++;

                                }

                                keys.RemoveLast();

                                if (keys.Count < 2)
                                {
                                    continue;
                                }

                                column++;

                                LinkedList<string> values = getExcelColumnValues(ws, column, tableStartLine, keys.Count);

                                if (values == null)
                                {
                                    continue;
                                }

                                #endregion

                                switch (TableRequestType)
                                {
                                    case TableRequestType.TABLE:

                                        #region Format JSON for TABLE

                                        bool first = true;

                                        List<Dictionary<string, string>> jsonObjects = new List<Dictionary<string, string>>();

                                        while (true)
                                        {
                                            if (!first)
                                            {
                                                values = null;

                                                values = getExcelColumnValues(ws, column, tableStartLine, keys.Count);

                                                if (values == null)
                                                {
                                                    break;
                                                }
                                            }

                                            jsonObjects.Add(putExcelValuesinJSON(keys, values));

                                            if (first)
                                            {
                                                first = false;
                                            }

                                            column++;
                                        }

                                        TreatRestAPIOutput(SimpleJson.SerializeObject(jsonObjects));

                                        #endregion

                                        break;
                                    case TableRequestType.LINE:

                                        #region Format JSON for LINE

                                        first = true;

                                        while (true)
                                        {
                                            if (!first)
                                            {
                                                values = null;

                                                values = getExcelColumnValues(ws, column, tableStartLine, keys.Count);

                                                if (values == null)
                                                {
                                                    break;
                                                }
                                            }

                                            TreatRestAPIOutput(SimpleJson.SerializeObject(putExcelValuesinJSON(keys, values)));

                                            if (first)
                                            {
                                                first = false;
                                            }

                                            column++;
                                        }

                                        #endregion

                                        break;
                                    default:
                                        throw new ArgumentException("Invalid Table Request Type");
                                }
                            }

                            #endregion

                            break;
                        case TableType.LINE:

                            #region Read Every Worksheet for Line

                            foreach (Excel.Worksheet ws in excelWorkbook.Sheets)
                            {
                                #region Check if valid and get keys

                                int line, tableStartColumn = 1;
                                string cell = "";

                                for (line = 1; line < 31; line++)
                                {
                                    for (tableStartColumn = 1; tableStartColumn < 31; tableStartColumn++)
                                    {
                                        cell = ws.Cells[line, tableStartColumn].Text;

                                        if (cell != "")
                                        {
                                            break;
                                        }
                                    }
                                    if (cell != "")
                                    {
                                        break;
                                    }
                                }

                                string key = "placeholder";
                                LinkedList<string> keys = new LinkedList<string>();

                                //int i = 1;

                                int column = tableStartColumn;

                                while (key != "")
                                {

                                    //key = ws.Cells[1, i].Text;

                                    key = ws.Cells[line, column].Text;

                                    keys.AddLast(key);
                                    //i++;
                                    column++;

                                }

                                keys.RemoveLast();

                                if (keys.Count < 2)
                                {
                                    continue;
                                }
                                line++;

                                LinkedList<string> values = getExcelLineValues(ws, line, tableStartColumn, keys.Count);

                                if (values == null)
                                {
                                    continue;
                                }

                                #endregion

                                switch (TableRequestType)
                                {
                                    case TableRequestType.TABLE:

                                        #region Format JSON for TABLE

                                        List<Dictionary<string, string>> jsonObjects = new List<Dictionary<string, string>>();

                                        bool first = true;

                                        while (true)
                                        {

                                            if (!first)
                                            {
                                                values = null;

                                                values = getExcelLineValues(ws, line, tableStartColumn, keys.Count);

                                                if (values == null)
                                                {
                                                    break;
                                                }
                                            }

                                            jsonObjects.Add(putExcelValuesinJSON(keys, values));

                                            if (first)
                                            {
                                                first = false;
                                            }

                                            line++;
                                        }

                                        TreatRestAPIOutput(SimpleJson.SerializeObject(jsonObjects));

                                        #endregion

                                        break;
                                    case TableRequestType.LINE:

                                        #region Format JSON for LINE
                                        first = true;

                                        while (true)
                                        {

                                            if (!first)
                                            {
                                                values = null;

                                                values = getExcelLineValues(ws, line, tableStartColumn, keys.Count);

                                                if (values == null)
                                                {
                                                    break;
                                                }
                                            }

                                            TreatRestAPIOutput(SimpleJson.SerializeObject(putExcelValuesinJSON(keys, values)));

                                            if (first)
                                            {
                                                first = false;
                                            }

                                            line++;
                                        }


                                        #endregion

                                        break;
                                    default:
                                        throw new ArgumentException("Invalid Table Request Type");
                                }

                            }

                            #endregion

                            break;
                        default:
                            throw new ArgumentException("Invalid Table Type");
                    }

                    #endregion
                }

            } catch(Exception e)
            {
                excelWorkbook.Close();
                excelApplication.Quit();

                ReleaseCOMObject(excelWorkbook);
                ReleaseCOMObject(excelApplication);

                throw e;
            }

            excelWorkbook.Close();
            excelApplication.Quit();

            ReleaseCOMObject(excelWorkbook);
            ReleaseCOMObject(excelApplication);
        }

        private static Dictionary<string, string> putExcelValuesinJSON(LinkedList<string> keys, LinkedList<string> values)
        {

            Dictionary<string, string> singleObject = new Dictionary<string, string>();
            for (int j = 0; j < keys.Count; j++)
            {
                singleObject.Add(keys.ElementAt(j), values.ElementAt(j));
            }

            return singleObject;

        }

        private static LinkedList<string> getExcelColumnValues(Excel.Worksheet ws, int column, int startLine, int keyAmount)
        {
            LinkedList<string> values = new LinkedList<string>();
            bool valid = false;

            for (int i = 0; i < keyAmount; i++)
            {
                values.AddLast(ws.Cells[startLine + i, column].Text);
                if (values.Last.Value != "")
                {
                    valid = true;
                }
            }

            if (!valid)
            {
                return null;
            }

            return values;
        }

        private static LinkedList<string> getExcelLineValues(Excel.Worksheet ws, int line, int startColumn, int keyAmount)
        {
            LinkedList<string> values = new LinkedList<string>();
            bool valid = false;

            for (int i = 0; i < keyAmount; i++)
            {
                values.AddLast(ws.Cells[line, startColumn + i].Text);
                if (values.Last.Value != "")
                {
                    valid = true;
                }
            }

            if (!valid)
            {
                return null;
            }

            return values;
        }

        private static void ReleaseCOMObject(Object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                //Quando apanhamos a exceção é importante ver o que a provocou, podemos então fazer um Debug
                System.Diagnostics.Debug.WriteLine("Exception releasing COM object." + ex.ToString());
                //Messagem mais técnica -> Chamar o ToString
                //Senão chamar o Message
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        #region Exclusive Broker

        public void RunBroker()
        {
            if (!InitializeBroker())
            {
                //Application.UseWaitCursor = false;
                throw new CouldNotConnectToRemoteException($"Could Not Connect to {Domain}");
            }
            switch (OutputType)
            {
                case OutputType.HTML:
                    InitializaHTMLFile();
                    break;
                case OutputType.RESTAPI:
                    break;

            }
            isRunning = true;
        }

        public void Stop()
        {
            switch (OutputType)
            {
                case OutputType.HTML:
                    if (broker.IsConnected)
                    {
                        broker.Disconnect();
                    }
                    AppendToHTMLFile("</table>");
                    sw.Dispose();
                    break;
            }
            isRunning = false;
        }

        private Boolean InitializeBroker()
        {
            bool success = false;
            string[] topics = { Topic };
            byte[] qosLevels = { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE };
            try
            {
                broker = new MqttClient(Domain);
                broker.Connect(Guid.NewGuid().ToString());
                if (!broker.IsConnected)
                {
                    MessageBox.Show("Verifique a disponiblidade do broker");
                }
                broker.Subscribe(topics, qosLevels);
                broker.MqttMsgPublishReceived += Broker_MqttMsgPublishReceived;
                success = true;
                MessageBox.Show("You are connected to broker (" + Name + ")!");
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        private void InitializaHTMLFile()
        {
            CreateFile();
            AppendToHTMLFile(GenerateHeaders());
        }

        private void CreateFile()
        {
            string fileToSave = $"{OutputFilePath}\\{OutputFileName}.html";
            try
            {
                sw = File.CreateText(fileToSave);
            }
            catch (Exception e)
            {
                throw new CouldNotCreateFileException("Could Not Create HTML File...");
            }

        }

        private string GenerateHeaders()
        {
            string headers = "";
            #region Initialize HTML with  table headers
            headers += "<table style='border: 1px solid black; margin-left: auto; margin-right: auto; border-collapse: collapse;'><tr>";
            foreach (string key in Keys)
            {
                headers += $"<th style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{key}</th>";
            }
            headers += "</tr>";

            #endregion
            return headers;
        }

        private void AppendToHTMLFile(string text)
        {
            try
            {
                sw.WriteLine(text);
            }
            catch (Exception e)
            {
                throw new CouldNotCreateFileException("Could Not Append HTML File");
            }
        }

        private void Broker_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            #region Received Data
            String strTemp = Encoding.UTF8.GetString(e.Message);
            string[] objects = null;
            int size = 1;
            if (ObjectSplitter.ToString() != "")
            {
                objects = strTemp.Split(new string[] { ObjectSplitter.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                size = objects.Length;
            }
            else
            {
                objects[0] = strTemp;
            }
            #endregion
            switch (OutputType)
            {
                case OutputType.HTML:
                    for(int i = 0; i < size; i++)
                    {
                        string[] values = objects[i].Split(new string[] { ValueSplitter.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                        string objectReceived = "";
                        objectReceived += "<tr>";
                        foreach (var value in values)
                        {
                            objectReceived += $"<td style='border: 1px solid black; border-collapse: collapse; padding: 15px; text-align: center'>{value}</td>";
                        }
                        objectReceived += "</tr>";
                        AppendToHTMLFile(objectReceived);
                    }
                    break;
                case OutputType.RESTAPI:
                    if (size == 1)
                    {
                        Dictionary<string, string> singleObject = new Dictionary<string, string>();
                        string[] values = objects[0].Split(new string[] { ValueSplitter.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < Keys.Length; j++)
                        {
                            singleObject.Add(Keys[j], values[j]);
                        }
                        try
                        {
                            TreatRestAPIOutput(SimpleJson.SerializeObject(singleObject));
                            MessageBox.Show("Object sent successfully");
                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message);
                        }
                        break;
                    }
                    List<Dictionary<string, string>> response = new List<Dictionary<string, string>>();
                    for (int i = 0; i < size; i++)
                    {
                        Dictionary<string, string> singleObject = new Dictionary<string, string>();
                        string[] values = objects[i].Split(new string[] { ValueSplitter.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < Keys.Length; j++)
                        {
                            singleObject.Add(Keys[j], values[j]);
                        }
                        response.Add(singleObject);
                    }
                    try
                    {
                        TreatRestAPIOutput(SimpleJson.SerializeObject(response));
                        MessageBox.Show("Objects sent successfully");
                    }
                    catch(Exception exception)
                    {
                        MessageBox.Show(exception.Message);
                    }
                    break;
            }
        }

        #endregion
    }
}
