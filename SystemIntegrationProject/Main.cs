﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SystemIntegrationProject
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonNewFlow_Click(object sender, EventArgs e)
        {
            using (var form = new NewFlow())
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    listBoxFlows.Items.Add(form.flow);
                }
            }
        }

        private void buttonImportFlow_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = "";
            openFileDialog.DefaultExt = ".xml";
            openFileDialog.Filter = "Xml Files|*.xml";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var filename = openFileDialog.FileName;
                Flow flow = ReadFromXmlFile<Flow>(filename);
                listBoxFlows.Items.Add(flow);
                flow.isRunning = false;
            }
        }

        public static T ReadFromXmlFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                reader = new StreamReader(filePath);
                return (T)serializer.Deserialize(reader);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        private void buttonSelectedFlow_Click(object sender, EventArgs e)
        {
            if (listBoxFlows.Items.Count == 0)
            {
                MessageBox.Show("You have no Flows! Please, create a new or import an existing one!");
                return;
            }
            if (listBoxFlows.SelectedIndex == -1)
            {
                MessageBox.Show("Select a Flow first!");
                return;
            }
            Flow flow = (Flow)listBoxFlows.SelectedItem;
            RunFlow(flow);
        }

        private void buttonRunAllFlows_Click(object sender, EventArgs e)
        {
            if (listBoxFlows.Items.Count == 0)
            {
                MessageBox.Show("You have no Flows! Please, create a new or import an existing one!");
                return;
            }
            foreach (Flow flow in listBoxFlows.Items)
            {
                RunFlow(flow);
            }
            
        }

        private void buttonDeleteFlow_Click(object sender, EventArgs e)
        {
            if (listBoxFlows.SelectedIndex == -1)
            {
                MessageBox.Show("Select a Flow first!");
                return;
            }
            var result = MessageBox.Show("Are you sure to delete this flow?",
                                     "Delete Flow",
                                     MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                listBoxFlows.Items.Remove(listBoxFlows.SelectedItem);
            }

        }

        private void RunFlow(Flow flow)
        {
            Application.UseWaitCursor = true;
            if (flow.InputType == InputType.BROKER)
            {
                var thread = new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    try
                    {
                        flow.Run();
                        Application.UseWaitCursor = false;
                    }
                    catch (Exception e)
                    {
                        Application.UseWaitCursor = false;
                        MessageBox.Show(e.Message);
                    }
                });
                thread.Start();
                while (thread.IsAlive)
                {
                    if (flow.InputType == InputType.BROKER && flow.isRunning)
                    {
                        buttonStopSelectedFlow.Enabled = true;
                        labelStatusRunning.Text = "Running";
                    }
                    else
                    {
                        labelStatusRunning.Text = "Not Running";
                    }
                    Application.UseWaitCursor = false;
                }
            }
            else
            {
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    try
                    {
                        flow.Run();
                        MessageBox.Show($"{flow.Name} finished with success!");
                        Application.UseWaitCursor = false;
                    }
                    catch (Exception e)
                    {
                        Application.UseWaitCursor = false;
                        MessageBox.Show(e.Message);
                    }
                }).Start();
            }
        }

        private void buttonEditFlow_Click(object sender, EventArgs e)
        {
            if (listBoxFlows.SelectedIndex == -1)
            {
                MessageBox.Show("Select a Flow first!");
                return;
            }
            using (var form = new NewFlow())
            {
                int index = listBoxFlows.SelectedIndex;
                form.flow = (Flow)listBoxFlows.SelectedItem;
                form.Text = "Edit Flow";
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    listBoxFlows.Items[index] = form.flow;
                }
            }
        }

        private void addFlowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonNewFlow.PerformClick();
        }

        private void importFlowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonImportFlow.PerformClick();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void buttonStopSelectedFlow_Click(object sender, EventArgs e)
        {
            Flow flow = (Flow)listBoxFlows.SelectedItem;
            if (flow.InputType == InputType.BROKER)
            {
                flow.Stop();
                buttonStopSelectedFlow.Enabled = false;
            }
            if (flow.isRunning)
            {
                labelStatusRunning.Text = "Running";
            }
            else
            {
                labelStatusRunning.Text = "Not Running";
            }
        }

        private void listBoxFlows_SelectedIndexChanged(object sender, EventArgs e)
        {
            Flow flow = (Flow)listBoxFlows.SelectedItem;
            if (flow == null)
            {
                return;
            }
            if (flow.InputType == InputType.BROKER && flow.isRunning)
            {
                buttonStopSelectedFlow.Enabled = true;
            }
            else
            {
                buttonStopSelectedFlow.Enabled = false;
            }
            if (flow.isRunning)
            {
                labelStatusRunning.Text = "Running";
            }
            else
            {
                labelStatusRunning.Text = "Not Running";
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            buttonStopSelectedFlow.Enabled = false;
        }
    }
}
