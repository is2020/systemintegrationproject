﻿namespace SystemIntegrationProject
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNewFlow = new System.Windows.Forms.Button();
            this.buttonImportFlow = new System.Windows.Forms.Button();
            this.listBoxFlows = new System.Windows.Forms.ListBox();
            this.buttonRunAllFlows = new System.Windows.Forms.Button();
            this.buttonSelectedFlow = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFlowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importFlowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonEditFlow = new System.Windows.Forms.Button();
            this.buttonDeleteFlow = new System.Windows.Forms.Button();
            this.progressBarFlow = new System.Windows.Forms.ProgressBar();
            this.buttonStopSelectedFlow = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelStatusRunning = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonNewFlow
            // 
            this.buttonNewFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFlow.Location = new System.Drawing.Point(41, 57);
            this.buttonNewFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonNewFlow.Name = "buttonNewFlow";
            this.buttonNewFlow.Size = new System.Drawing.Size(283, 58);
            this.buttonNewFlow.TabIndex = 0;
            this.buttonNewFlow.Text = "New Flow";
            this.buttonNewFlow.UseVisualStyleBackColor = true;
            this.buttonNewFlow.Click += new System.EventHandler(this.buttonNewFlow_Click);
            // 
            // buttonImportFlow
            // 
            this.buttonImportFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImportFlow.Location = new System.Drawing.Point(868, 764);
            this.buttonImportFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonImportFlow.Name = "buttonImportFlow";
            this.buttonImportFlow.Size = new System.Drawing.Size(159, 58);
            this.buttonImportFlow.TabIndex = 1;
            this.buttonImportFlow.Text = "Import Flow";
            this.buttonImportFlow.UseVisualStyleBackColor = true;
            this.buttonImportFlow.Click += new System.EventHandler(this.buttonImportFlow_Click);
            // 
            // listBoxFlows
            // 
            this.listBoxFlows.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxFlows.FormattingEnabled = true;
            this.listBoxFlows.ItemHeight = 25;
            this.listBoxFlows.Location = new System.Drawing.Point(41, 143);
            this.listBoxFlows.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBoxFlows.Name = "listBoxFlows";
            this.listBoxFlows.Size = new System.Drawing.Size(571, 654);
            this.listBoxFlows.TabIndex = 3;
            this.listBoxFlows.SelectedIndexChanged += new System.EventHandler(this.listBoxFlows_SelectedIndexChanged);
            // 
            // buttonRunAllFlows
            // 
            this.buttonRunAllFlows.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRunAllFlows.Location = new System.Drawing.Point(649, 700);
            this.buttonRunAllFlows.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonRunAllFlows.Name = "buttonRunAllFlows";
            this.buttonRunAllFlows.Size = new System.Drawing.Size(379, 58);
            this.buttonRunAllFlows.TabIndex = 4;
            this.buttonRunAllFlows.Text = "Run All Flows";
            this.buttonRunAllFlows.UseVisualStyleBackColor = true;
            this.buttonRunAllFlows.Click += new System.EventHandler(this.buttonRunAllFlows_Click);
            // 
            // buttonSelectedFlow
            // 
            this.buttonSelectedFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectedFlow.Location = new System.Drawing.Point(649, 764);
            this.buttonSelectedFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelectedFlow.Name = "buttonSelectedFlow";
            this.buttonSelectedFlow.Size = new System.Drawing.Size(213, 58);
            this.buttonSelectedFlow.TabIndex = 5;
            this.buttonSelectedFlow.Text = "Run Selected Flow";
            this.buttonSelectedFlow.UseVisualStyleBackColor = true;
            this.buttonSelectedFlow.Click += new System.EventHandler(this.buttonSelectedFlow_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1069, 28);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addFlowToolStripMenuItem,
            this.importFlowToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(75, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // addFlowToolStripMenuItem
            // 
            this.addFlowToolStripMenuItem.Name = "addFlowToolStripMenuItem";
            this.addFlowToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.addFlowToolStripMenuItem.Text = "Add Flow";
            this.addFlowToolStripMenuItem.Click += new System.EventHandler(this.addFlowToolStripMenuItem_Click);
            // 
            // importFlowToolStripMenuItem
            // 
            this.importFlowToolStripMenuItem.Name = "importFlowToolStripMenuItem";
            this.importFlowToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.importFlowToolStripMenuItem.Text = "Import Flow";
            this.importFlowToolStripMenuItem.Click += new System.EventHandler(this.importFlowToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // buttonEditFlow
            // 
            this.buttonEditFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditFlow.Location = new System.Drawing.Point(329, 57);
            this.buttonEditFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEditFlow.Name = "buttonEditFlow";
            this.buttonEditFlow.Size = new System.Drawing.Size(131, 58);
            this.buttonEditFlow.TabIndex = 7;
            this.buttonEditFlow.Text = "Edit Flow";
            this.buttonEditFlow.UseVisualStyleBackColor = true;
            this.buttonEditFlow.Click += new System.EventHandler(this.buttonEditFlow_Click);
            // 
            // buttonDeleteFlow
            // 
            this.buttonDeleteFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteFlow.Location = new System.Drawing.Point(467, 57);
            this.buttonDeleteFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDeleteFlow.Name = "buttonDeleteFlow";
            this.buttonDeleteFlow.Size = new System.Drawing.Size(147, 58);
            this.buttonDeleteFlow.TabIndex = 8;
            this.buttonDeleteFlow.Text = "Delete Flow";
            this.buttonDeleteFlow.UseVisualStyleBackColor = true;
            this.buttonDeleteFlow.Click += new System.EventHandler(this.buttonDeleteFlow_Click);
            // 
            // progressBarFlow
            // 
            this.progressBarFlow.Location = new System.Drawing.Point(648, 609);
            this.progressBarFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBarFlow.Name = "progressBarFlow";
            this.progressBarFlow.Size = new System.Drawing.Size(379, 23);
            this.progressBarFlow.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarFlow.TabIndex = 9;
            this.progressBarFlow.Visible = false;
            // 
            // buttonStopSelectedFlow
            // 
            this.buttonStopSelectedFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStopSelectedFlow.Location = new System.Drawing.Point(649, 638);
            this.buttonStopSelectedFlow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonStopSelectedFlow.Name = "buttonStopSelectedFlow";
            this.buttonStopSelectedFlow.Size = new System.Drawing.Size(379, 58);
            this.buttonStopSelectedFlow.TabIndex = 10;
            this.buttonStopSelectedFlow.Text = "Stop Selected Flow";
            this.buttonStopSelectedFlow.UseVisualStyleBackColor = true;
            this.buttonStopSelectedFlow.Click += new System.EventHandler(this.buttonStopSelectedFlow_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Location = new System.Drawing.Point(643, 74);
            this.labelStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(206, 25);
            this.labelStatus.TabIndex = 11;
            this.labelStatus.Text = "Status (Broker Flows):";
            // 
            // labelStatusRunning
            // 
            this.labelStatusRunning.AutoSize = true;
            this.labelStatusRunning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatusRunning.Location = new System.Drawing.Point(864, 78);
            this.labelStatusRunning.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStatusRunning.Name = "labelStatusRunning";
            this.labelStatusRunning.Size = new System.Drawing.Size(27, 20);
            this.labelStatusRunning.TabIndex = 12;
            this.labelStatusRunning.Text = "---";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 890);
            this.Controls.Add(this.labelStatusRunning);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonStopSelectedFlow);
            this.Controls.Add(this.progressBarFlow);
            this.Controls.Add(this.buttonDeleteFlow);
            this.Controls.Add(this.buttonEditFlow);
            this.Controls.Add(this.buttonSelectedFlow);
            this.Controls.Add(this.buttonRunAllFlows);
            this.Controls.Add(this.listBoxFlows);
            this.Controls.Add(this.buttonImportFlow);
            this.Controls.Add(this.buttonNewFlow);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Main";
            this.Text = "System Integration";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNewFlow;
        private System.Windows.Forms.Button buttonImportFlow;
        private System.Windows.Forms.ListBox listBoxFlows;
        private System.Windows.Forms.Button buttonRunAllFlows;
        private System.Windows.Forms.Button buttonSelectedFlow;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFlowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importFlowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button buttonEditFlow;
        private System.Windows.Forms.Button buttonDeleteFlow;
        private System.Windows.Forms.ProgressBar progressBarFlow;
        private System.Windows.Forms.Button buttonStopSelectedFlow;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelStatusRunning;
    }
}

