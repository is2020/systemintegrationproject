﻿/**
 * Data Sources Converter
 * Integração de Sistemas
 * Membros - Alexandre Santos, André Silva, César Subtil, Miguel Santos
 * 
 * Table Type Enum -> Enumeration with all Table Types (Read table with keys by LINE vs by COLUMN)
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemIntegrationProject
{
    public enum TableType
    {
        LINE,
        COLUMN
    }
}
